// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Runtime/Online/HTTP/Public/Http.h"
#include "Json.h"
#include "JsonUtilities.h"

#include "Engine.h"

#include "HttpService.generated.h"

USTRUCT()
struct FServiceParams
{
	GENERATED_BODY()
private:
	FString m_sParams;
	int m_counterParams;
public:
	void Add(FString key, FString value)
	{
		if (m_counterParams != 0)
			m_sParams += "&";


		m_sParams += key + "=" + value;
		m_counterParams++;
	}
	FString GetParams()
	{
		return m_sParams; 
	}

	FServiceParams()
	{
		m_sParams = "";
		m_counterParams = 0;
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUserCreatedCallback, FString, Data, bool, Result);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLeaderboardLoadedCallback, FString, Data, bool, Result);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FIsUsernameEmpty, bool, Result);

UCLASS()
class FLAPPYBIRDS_API AHttpService : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHttpService();
	~AHttpService();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintCallable, Category = "HttpService:Leaderboard")
	void CreateUser(FString username);
	UFUNCTION(BlueprintCallable, Category = "HttpService:Leaderboard")
	void GetLeaderboard();
	UFUNCTION(BlueprintCallable, Category = "HttpService:Leaderboard")
	void GetLastPlayerScore(FString username);
	UFUNCTION(BlueprintCallable, Category = "HttpService:Leaderboard")
	void UpdateScore(FString username, int score);
	UFUNCTION(BlueprintCallable, Category = "HttpService:Leaderboard")
	void CallUsername();
protected:
	TSharedRef<IHttpRequest> PostRequest(FString Subroute, FServiceParams params);
	void SetRequestHeaders(TSharedRef<IHttpRequest>& Request);
	void Send(TSharedRef<IHttpRequest>& Request);
	TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);
protected:
	void OnUserCreationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnLeaderboardLoad(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnScoreUpdated(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void OnPlayerScoreGot(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
private:
	FHttpModule* m_pServiceModule = nullptr;
	FString m_sBaseUrl = "http://146.66.175.175:80/game_api/";
	FString m_fUsername;
private:
	UPROPERTY(BlueprintAssignable, Category = "HttpService:Events")
	FOnUserCreatedCallback OnUserCreated;
	UPROPERTY(BlueprintAssignable, Category = "HttpService:Events")
	FOnLeaderboardLoadedCallback OnLeaderboaedLoaded;
	UPROPERTY(BlueprintAssignable, Category = "HttpService:Events")
	FIsUsernameEmpty IsUsernameEmpty;

};
