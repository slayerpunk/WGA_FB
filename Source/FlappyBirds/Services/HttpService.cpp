// Fill out your copyright notice in the Description page of Project Settings.


#include "HttpService.h"

// Sets default values
AHttpService::AHttpService()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	m_pServiceModule = &FHttpModule::Get();
}

AHttpService::~AHttpService()
{
	
}

// Called when the game starts or when spawned
void AHttpService::BeginPlay()
{
	Super::BeginPlay();
	FString FullPath = FPaths::ProjectContentDir() + "user.json";
	FFileHelper::LoadFileToString(m_fUsername, *FullPath);
}

// Called every frame
void AHttpService::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHttpService::CreateUser(FString username)
{
	FServiceParams params;
	params.Add("Username", username);

	m_fUsername = username;

	TSharedRef<IHttpRequest> newUserRequest = PostRequest("new_user.php", params);
	newUserRequest->OnProcessRequestComplete().BindUObject(this, &AHttpService::OnUserCreationResponse);

	Send(newUserRequest);
}

void AHttpService::GetLeaderboard()
{
	TSharedRef<IHttpRequest> leaderboardRequest = PostRequest("get_players.php", FServiceParams());
	leaderboardRequest->OnProcessRequestComplete().BindUObject(this, &AHttpService::OnLeaderboardLoad);

	Send(leaderboardRequest);
}

void AHttpService::GetLastPlayerScore(FString username)
{
	FServiceParams params;
	params.Add("Username", m_fUsername);

	TSharedRef<IHttpRequest> playerScoreRequest = PostRequest("get_last_score.php", params);
	playerScoreRequest->OnProcessRequestComplete().BindUObject(this, &AHttpService::OnPlayerScoreGot);

	Send(playerScoreRequest);
}

void AHttpService::UpdateScore(FString username, int score)
{
	FServiceParams params;
	params.Add("Username", m_fUsername);
	params.Add("Score", FString::FromInt(score));

	TSharedRef<IHttpRequest> scoreUpdateRequest = PostRequest("update_score.php", params);
	scoreUpdateRequest->OnProcessRequestComplete().BindUObject(this, &AHttpService::OnScoreUpdated);

	Send(scoreUpdateRequest);
}

void AHttpService::CallUsername()
{
	if (IsUsernameEmpty.IsBound())
	{
		IsUsernameEmpty.Broadcast(m_fUsername.IsEmpty());
	}
}

TSharedRef<IHttpRequest> AHttpService::PostRequest(FString Subroute, FServiceParams params)
{
	TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("POST");
	Request->SetContentAsString(params.GetParams());
	return Request;
}

void AHttpService::SetRequestHeaders(TSharedRef<IHttpRequest>& Request)
{
	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/x-www-form-urlencoded"));
}

void AHttpService::Send(TSharedRef<IHttpRequest>& Request)
{
	Request->ProcessRequest();
}

TSharedRef<IHttpRequest> AHttpService::RequestWithRoute(FString Subroute)
{
	TSharedRef<IHttpRequest> Request = m_pServiceModule->CreateRequest();
	Request->SetURL(m_sBaseUrl + Subroute);
	SetRequestHeaders(Request);
	return Request;
}

void AHttpService::OnUserCreationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (!bWasSuccessful) return;

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		FString data = JsonObject->GetStringField("message");
		int32 result = JsonObject->GetIntegerField("return");

		FString FullPath = FPaths::ProjectContentDir() + "user.json";

		if ((bool)result)
		{
			FFileHelper::SaveStringToFile(m_fUsername, *FullPath);
		}
		
		if (OnUserCreated.IsBound())
		{
			OnUserCreated.Broadcast(data, (bool)result);
		}
	}
}

void AHttpService::OnLeaderboardLoad(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	if (!bWasSuccessful) return;

	//Create a pointer to hold the json serialized data
	TSharedPtr<FJsonObject> JsonObject;

	//Create a reader pointer to read the json data
	TSharedRef<TJsonReader<>> Reader = TJsonReaderFactory<>::Create(Response->GetContentAsString());

	//Deserialize the json data given Reader and the actual object to deserialize
	if (FJsonSerializer::Deserialize(Reader, JsonObject))
	{
		//Get the value of the json object by field name
		TArray<TSharedPtr<FJsonValue>> arr = JsonObject->GetArrayField("message");
		int32 result = JsonObject->GetIntegerField("return");

		FString msg = "";

		for (int i = 0; i < arr.Num(); i++)
		{
			int num = i;
			TSharedPtr<FJsonObject> it = arr[i]->AsObject();
			msg += FString::FromInt(++num) + ". " + it->GetStringField("username") + " : " + FString::FromInt(it->GetIntegerField("score")) + "\n";
		}

		if (OnLeaderboaedLoaded.IsBound())
		{
			OnLeaderboaedLoaded.Broadcast(msg, (bool)result);
		}
	}
}

void AHttpService::OnScoreUpdated(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
}

void AHttpService::OnPlayerScoreGot(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
}

